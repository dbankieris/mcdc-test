#include <iostream>
#include <iomanip>
#include "ChildTemplate.hh"
#include "Example.hh"
#include "Utilities.hh"

namespace example {
Foo1::Foo1(int i) {(void)i;}
Foo2::Foo2(const int i) {(void)i;}
Foo3::Foo3(const int& i) {(void)i;}
Foo4::Foo4(std::string s) {(void)s;}
Foo5::Foo5(const std::string s) {(void)s;}
Foo6::Foo6(const std::string& s) {(void)s;}
void defineTest1();
void defineTest2();
}

int main(int, char**) {
    // coverage for lambdas?
    auto callback = [](int a, double b, char c, example::Foo::Bar d) {
        std::cout << std::fixed << std::setprecision(1)
                  << "Got " << a << ' ' << b << ' ' << c << ' ' << d << "\n";
    };

    // any indication this statement is executed but the lambda is never called?
    auto neverCalled =
        [&](int nope){
            std::cout << nope << " Never printed!\n";
        };

    srand(time(NULL));
    example::AsynchronouslyFetchedValue<int> i{};
    example::AsynchronouslyFetchedValue<double> d{};
    example::AsynchronouslyFetchedValue<char> c{};
    example::AsynchronouslyFetchedValue<example::Foo::Bar> f{};

    // coverage for variadic templates and crazy template metaprogramming?
    getValues(callback, i, d, c, f);

    // coverage for overriding and overridden functions?
    i.foo(example::Foo::Bar::A);

    // coverage for template specializations?
    const uint8_t bytes[] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};
    example::bytesToType<int>(bytes);
    example::bytesToType<float>(bytes);
    example::bytesToType<double>(bytes);

    // support for default arguments?
    example::Foo1 f1;
    example::Foo2 f2;
    example::Foo3 f3;
    example::Foo4 f4;
    example::Foo5 f5;
    example::Foo6 f6;

    f1.foo();
    f2.foo();
    f3.foo();
    f4.foo();
    f5.foo();
    f6.foo();

    // support for template stuff?
    example::ChildTemplate<> childTemplate{1};

    // handles #defines correctly?
    example::defineTest1();
    example::defineTest2();

    // support for dynamic_cast?
    //if (dynamic_cast<example::Foo*>(&i)) {
        std::cout << "Test done!\n";
    //}

    return 0;
}
