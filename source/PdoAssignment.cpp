#include "PdoAssignment.hh"

namespace example {

PdoAssignment::PdoAssignment(const std::vector<std::reference_wrapper<ProcessDataObject> >& pdos) :
  processDataObjects(pdos) {}

PdoAssignment::PdoAssignment(
  const std::function<const std::vector<std::reference_wrapper<ProcessDataObject> >()>& getPdos) :
  PdoAssignment(getPdos()) {}

}
