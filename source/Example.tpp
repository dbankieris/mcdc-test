namespace example {

template <class T>
ChildTemplate<T>::ChildTemplate(const Foo& foo) :
    BaseTemplate<T>(foo) {}

template <class T>
ChildTemplate<T>::~ChildTemplate() {}

}
