#include "Utilities.hh"

namespace example {

template <>
float bytesToType(const uint8_t* bytes) {
    return static_cast<float>(bytesToType<uint32_t>(bytes));
}

template <>
double bytesToType(const uint8_t* bytes) {
    return static_cast<double>(bytesToType<uint64_t>(bytes));
}

}
