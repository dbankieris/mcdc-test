#define EXAMPLE_DEFINE_TEST2
#include "define_test/DefineTest.hh"
#undef EXAMPLE_DEFINE_TEST2

#include <iostream>

namespace example {

void defineTest2() {
#ifdef EXAMPLE_TEST1
    // this should be uncovered
    std::cout << "defineTest2 TEST1\n";
#endif
#ifdef EXAMPLE_TEST2
    // this should be covered
    std::cout << "defineTest2 TEST2\n";
#endif
}

}
