#define EXAMPLE_DEFINE_TEST1
#include "define_test/DefineTest.hh"
#undef EXAMPLE_DEFINE_TEST1

#define EXAMPLE_TEST1

#include <iostream>

namespace example {

void defineTest1() {
#ifdef EXAMPLE_TEST1
    // this should be covered
    std::cout << "defineTest1 TEST1\n";
#endif
#ifdef EXAMPLE_TEST2
    // this should be uncovered
    std::cout << "defineTest1 TEST2\n";
#endif
}

}
