#include <tuple>

#if __cplusplus < 201402L // c++14

namespace std {

/**
 * C++11 implementation of std::index_sequence and std::make_index_sequence,
 * which were introduced in C++14.
 *
 * See https://stackoverflow.com/a/49672613 for an explanation.
 */
template <std::size_t...>
struct index_sequence {};

template <std::size_t N, std::size_t... Next>
struct make_index_sequence : make_index_sequence<N - 1, N - 1, Next...> {};

template <std::size_t... Next>
struct make_index_sequence<0, Next...> : index_sequence<Next...> {};

}

#else
#include <utility>
#endif


#if __cplusplus < 201703L // c++17

namespace std {

/**
 * C++11 implementation of std::apply, which was introduced in C++17.
 * Adapted from https://en.cppreference.com/w/cpp/utility/apply
 */
template <class F, class Tuple, std::size_t... I>
void apply(F&& f, Tuple&& t, std::index_sequence<I...>) {
    f(std::get<I>(std::forward<Tuple>(t))...);
}

template <class F, class Tuple>
void apply(F&& f, Tuple&& t) {
    apply(std::forward<F>(f), std::forward<Tuple>(t),
      std::make_index_sequence<
        std::tuple_size<typename std::remove_reference<Tuple>::type>::value>{});
}

}

#endif


namespace example {

template <class T>
T bytesToType(const uint8_t* bytes) {
    // TODO: this assumes the native endianess is little
    T result = bytes[sizeof(T) - 1];
    for (int i = static_cast<int>(sizeof(T)) - 2; i >= 0; --i) {
        result = (result << 8) | bytes[i];
    }
    return result;
}

template <std::size_t N, class F, class... Ts>
typename std::enable_if<N == sizeof...(Ts)>::type getValues(const F& callback,
  const std::tuple<const AsynchronouslyFetchedValue<Ts>&...>& variables,
  const std::tuple<Ts...>& values) {
    (void)variables;
    std::apply(callback, values);
}

template <std::size_t N, class F, class... Ts>
  typename std::enable_if < N<sizeof...(Ts)>::type getValues(const F& callback,
    const std::tuple<const AsynchronouslyFetchedValue<Ts>&...>& variables,
    std::tuple<Ts...> values = {}) {
    std::get<N>(variables).get(
      [=](typename std::tuple_element<N, std::tuple<Ts...> >::type value) mutable {
          std::get<N>(values) = value;
          getValues<N + 1>(callback, variables, values);
      });
}

template <class F, class... Ts>
void getValues(const F& callback, const AsynchronouslyFetchedValue<Ts>&... variables) {
    getValues<0>(callback, std::forward_as_tuple(variables...));
}

}
