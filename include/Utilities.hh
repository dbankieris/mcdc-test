#ifndef EXAMPLE_Utilities_HH
#define EXAMPLE_Utilities_HH

#include <cstddef>
#include <string>
#include <type_traits>

#include "Example.hh"

namespace example {

// postfix operator support for enums
template <class T, typename std::enable_if<std::is_enum<T>::value>::type* = nullptr>
T operator++(T& t, int) {
    T original{t};
    t = static_cast<T>(static_cast<typename std::underlying_type<T>::type>(t) + 1);
    return original;
}

// stream support for enums
template <class T, typename std::enable_if<std::is_enum<T>::value>::type* = nullptr>
std::ostream& operator<<(std::ostream& os, const T& t) {
   os << static_cast<typename std::underlying_type<T>::type>(t);
   return os;
}

/**
 * Extracts a value of the specified type from @a bytes. The caller must ensure @a bytes points to
 * at least @a sizeof(T) valid bytes.
 *
 * @tparam T the type of data to extract
 *
 * @param bytes the starting byte
 *
 * @return the value
 */
template <class T>
T bytesToType(const uint8_t* bytes);

/**
 * Extracts a @a float from @a bytes. The caller must ensure @a bytes points to
 * at least @a sizeof(float) valid bytes.
 *
 * @param bytes the starting byte
 *
 * @return the value
 */
template <>
float bytesToType(const uint8_t* bytes);

/**
 * Extracts a @a double from @a bytes. The caller must ensure @a bytes points to
 * at least @a sizeof(double) valid bytes.
 *
 * @param bytes the starting byte
 *
 * @return the value
 */
template <>
double bytesToType(const uint8_t* bytes);

template <class F, class... Ts>
void getValues(const F& callback, const Ts&... variables);

}

#include "Utilities.tpp"

#endif
