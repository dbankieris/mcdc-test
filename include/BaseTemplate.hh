#ifndef EXAMPLE_BaseTemplate_HH
#define EXAMPLE_BaseTemplate_HH

#include "PdoAssignment.hh"

namespace example {

template <class T = PdoAssignment>
class BaseTemplate {
    public:

        T t;

        const uint32_t id;

        BaseTemplate(uint32_t id);

        ~BaseTemplate() {};
};

}

#include "BaseTemplate.tpp"

#endif
