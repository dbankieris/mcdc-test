#ifndef EXAMPLE_Example_HH
#define EXAMPLE_Example_HH

#include <array>
#include <functional>

// support for namespaces?
namespace example {

class Foo {
    public:
        // support for nested types?
        // support for scoped and based enums?
        enum class Bar : uint16_t {A, B, C};

        // coverage for desctructors?
        virtual ~Foo() {}

        // coverage for inline and virtual functions?
        virtual int foo(Bar bar) {
            int value;

            // coverage for switches?
            switch (bar) {
                case Bar::A:
                    value = 0;
                    break;
                case Bar::B:
                    value = 1;
                    break;
                default:
                    value = 2;
            }

            return  value;
        }

};


// coverage for templates?
// support for default template arguments?
// support for inheritance?
template <class T, size_t N = 250>
class AsynchronouslyFetchedValue : public Foo {

    // support for static_assert?
    static_assert(N <= 250, "N must be less than 250");

    public:
        // support for STL containers?
        std::array<T, N> things{};

        // coverage for desctructors?
        ~AsynchronouslyFetchedValue() {}

        AsynchronouslyFetchedValue() : Foo::Foo() {
            // coverage for uniform initialization?
            T t{};


            // coverage for range-based loops?
            for (T& thing : things) {
                thing = t++;
            }
        }

        // support for std::function?
        void get(std::function<void(T)> callback) const {
            callback(things.at(rand() % N));
        }

        // coverage for overriding and overridden functions?
        int foo(Bar bar) override {
            // coverage for ternary operator?
            return Foo::foo(bar) < 2 ? 10 : 20;
        }
};

// support for default default arguments?
class Foo1 {
    public:
    Foo1(int i = 1);
    void foo (int i = 1) {(void)i;}
};
class Foo2 {
    public:
    Foo2(const int i = 1);
    void foo(const int i = 1) {(void)i;}
};
class Foo3 {
    public:
    Foo3(const int& i = 1);
    void foo(const int& i = 1) {(void)i;};
};
class Foo4 {
    public:
    Foo4(std::string s = "Foo");
    void foo(std::string s = "Foo") {(void)s;};
};
class Foo5 {
    public:
    Foo5(const std::string s = "Foo");
    void foo(const std::string s = "Foo") {(void)s;};
};
class Foo6 {
    public:
    Foo6(const std::string& s = "Foo");
    void foo(const std::string& s = "Foo") {(void)s;};
};

}

#endif
