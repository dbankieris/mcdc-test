#ifndef EXAMPLE_ChildTemplate_HH
#define EXAMPLE_ChildTemplate_HH

#include "BaseTemplate.hh"
#include "ProcessDataObject.hh"

namespace example {

namespace pdos { namespace child_template {

class DefaultPdoAssignment : public PdoAssignment {

    public:
        ProcessDataObject pdo1{"1"};
        ProcessDataObject pdo2{"2"};
        ProcessDataObject pdo3{"3"};

        DefaultPdoAssignment();

};

}}

template <class T = pdos::child_template::DefaultPdoAssignment>
class ChildTemplate : public BaseTemplate<T> {

    public:

        ChildTemplate(uint32_t id) :
            BaseTemplate<T>(id) {}

        ~ChildTemplate() {};

        class InnerClass {
            public:
                InnerClass() {};
                ~InnerClass() {};
        } innerClass;
};

}

#include "ChildTemplate.tpp"

#endif
