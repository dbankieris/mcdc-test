#ifndef EXAMPLE_ProcessDataObject_HH
#define EXAMPLE_ProcessDataObject_HH

#include <string>

namespace example {

class ProcessDataObject {

  public:
    std::string name;

    ProcessDataObject(const std::string& name);

};

}

#endif
