#ifndef EXAMPLE_PdoAssignment_HH
#define EXAMPLE_PdoAssignment_HH

#include <functional>
#include <vector>

namespace example {

class ProcessDataObject;

class PdoAssignment {

  public:
    const std::vector<std::reference_wrapper<ProcessDataObject> >
      processDataObjects;

    explicit PdoAssignment(
      const std::vector<std::reference_wrapper<ProcessDataObject> >& pdos = {});

    explicit PdoAssignment(
      const std::function<const std::vector<std::reference_wrapper<ProcessDataObject> >()>&
        getPdos);

  private:
    void operator=(const PdoAssignment&);
};

}

#endif
